<?php
/**
 * @PSR-0: Env\Network\Response
 * ============================
 *
 * @Filename Response.php
 *
 * @author Pablo Adrian Samudia <p.a.samu@gmail.com>
 */

namespace Env\Network;

class Response extends \Env\Object
{
    private $header;

    public $body;

    public $type = 'html';

    public function render()
    {
        \Env\Data\Definition\Mime::setType( $this );
        echo $this->body;
    }
}
