<?php
/**
 * @PSR-0: Env\Network\Dispatch
 * =============================
 *
 * @Filename Dispatch.php
 *
 * @author Pablo Adrian Samudia <p.a.samu@gmail.com>
 */

namespace Env\Network;

use Env\Object;

class Dispatch extends Object
{
    public static function run( Request $Request, Response $Response )
    {

    }
}
