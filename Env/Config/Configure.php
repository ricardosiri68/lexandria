<?php
/**
 * @PSR-0: Env\Config\Configure
 * =============================
 *
 * @Filename Configure.php
 *
 * @author Pablo Adrian Samudia <p.a.samu@gmail.com>
 */

namespace Env\Config;

class Configure extends \Env\Data\Collection
{
    
    public function get( $key, $default = false )
    {
        return $this->__find( $key );
    }
}
